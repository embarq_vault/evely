"use strict";

const UserModelFactory = require('./user.model');

const validEmailRegexp = /\w.+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/;

module.exports = function(db) {
  const UserModel = UserModelFactory(db);

  const create = function(req, res) {
    UserModel.create(req.body, function(err, user) {
      if (err) {
        console.log(err);
        return res
          .status(503)
      }

      return res
        .status(201)
        .send({
          email: user.email,
          name: user.name
        });
    });
  }

  const get = function(req, res) {
    const { email } = req.query;

    if (email != null || email != '') {
      if (!validEmailRegexp.test(email)) {
        return res
          .status(400)
          .send({
            message: 'Invalid value of "email" parameter'
          });
      }

      return UserModel.get(email, function(err, [ user ]) {
        if (err != null) {
          console.error(err);

          return res
            .status(503)
            .send(err);
        } else if (user == null) {
          const message = `User with email "${ email }" not found`;
          console.error(message);

          return res
            .status(404)
            .send({ message });
        }

        return res
          .status(200)
          .send(user);
      });
    } else {
      res.status(400);
    }
  }

  return { create, get }
}
