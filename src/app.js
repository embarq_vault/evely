"use strict";

const express = require('express');
const bodyParser = require('body-parser');
const seraph = require('seraph');
const passport = require('passport');

const users = require('./users/user.router');

module.exports = function() {
  const db = seraph({
    user: 'nanboard',
    pass: 'nanboard'
  });

  const app = express();

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: false
  }));

  app.use(passport.initialize());

  app.use('/users', users(db));

  app.listen(3000, 'localhost', () => console.log('Server started'));
}
