const router = require('express').Router();

module.exports = function(db) {
  const controller = require('./user.controller')(db);

  router.get('/', controller.get);
  router.post('/', controller.create);

  return router;
}
