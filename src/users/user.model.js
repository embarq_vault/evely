"use strict";

const seraphModel = require('seraph-model');

module.exports = function(db) {
  const UserModel = seraphModel(db, 'user');

  UserModel.schema = {
    email: {
      type: String,
      required: true,
      match: /\w.+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/
    },
    password: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true,
      trim: true,
      match: /[a-zA-Z]+/
    }
  };

  const create = function(user, done) {
    UserModel.save(user, 'User', (err, user) => done(err, user));
  }

  const get = function(email, done) {
    UserModel.where({ email }, (err, user) => done(err, user));
  }

  return { create, get };
}
