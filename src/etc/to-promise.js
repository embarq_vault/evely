module.exports = function() {
  Function.prototype.toPromise = function toPromise(params, options) {
    return new Promise((function(self) {
      return function(resolve, reject) {
        var callback = function(err, result) {
          if (err) console.log(err);
          return err ? reject(err) : resolve(result);
        };

        if (params) {
          if (options) {
            return self.call(self, params, options, callback);
          }
          return self.call(self, params, callback);
        }
        return self.call(self, callback);
      }
    })(this));
  }
}
